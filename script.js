var tanya = true;

while (tanya) {
  // menangkap pilihan player
  var pemain = prompt(`
  --------------------* Game Suit *--------------------
  
  Pilih salah satu : 
  
  - gunting
  - batu 
  - kertas 
  --------------------------------------------------------
  
  `);

  // menangkap pilihan komputeruter
  // membangkitkan bilangan random
  var komputer = Math.random();
  if (komputer < 0.34) {
    komputer = "gunting";
  } else if (komputer >= 0.34 && komputer < 0.67) {
    komputer = "batu";
  } else {
    komputer = "kertas";
  }

  var hasil = "";
  // menentukan rules
  if (pemain == komputer) {
    hasil = "SERI!";
  } else if (pemain == "batu") {
    hasil = komputer == "gunting" ? "MENANG!" : "KALAH!";
  } else if (pemain == "gunting") {
    hasil = komputer == "batu" ? "KALAH!" : "MENANG!";
  } else if (pemain == "kertas") {
    hasil = komputer == "gunting" ? "KALAH!" : "MENANG!";
  } else {
    hasil = "Kamu memasukkan pilihan yang salah!";
  }

  // tampilkan hasilnya
  alert("Kamu memilih : " + pemain + "\nKomputer memilih : " + komputer + "\n\nMaka hasilnya : Kamu " + hasil);

  tanya = confirm("Mau bermain lagi?");
}

alert("Terima kasih sudah bermain :)");
